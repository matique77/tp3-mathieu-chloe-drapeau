package model;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName ="langues")
public class Langues {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "pays")
    private String pays;

    @ColumnInfo(name = "codeDrapeau")
    private String codeDrapeau;

    @ColumnInfo(name = "langue")
    private String langue;

    public Langues()
    {

    }

    public Langues(int id, String codeDrapeau, String pays, String langue) {
        this.id = id;
        this.pays = pays;
        this.langue = langue;
        this.codeDrapeau=codeDrapeau;
    }

    public Langues(String codeDrapeau, String pays, String langue) {
        this.pays = pays;
        this.langue = langue;
        this.codeDrapeau=codeDrapeau;
    }

    public String getCodeDrapeau() {
        return codeDrapeau;
    }

    public static  String langueToStr(Langues langues)
    {
        return langues.getCodeDrapeau() + "," + langues.getPays() + "," + langues.getLangue();
    }

    public static  Langues strToLangue(String langue)
    {
        String[] info = langue.split(",");

        return new Langues(info[0], info[1], info[2]);
    }

    public void setCodeDrapeau(String codeDrapeau) {
        this.codeDrapeau = codeDrapeau;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getLangue() {
        return langue;
    }

    public void setLangue(String langue) {
        this.langue = langue;
    }
}