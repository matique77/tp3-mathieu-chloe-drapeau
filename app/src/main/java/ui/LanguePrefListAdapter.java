package ui;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dinfogarneau.cours02e.exercices.tp2_mobile.R;

import java.util.ArrayList;
import java.util.List;

import model.Langues;

/**
 * Adapteur gérant l'affichage des langues dans la liste de langues
 * de préférance.
 *
 * Note : Similaire au LangueListAdapter à quelques différences.
 */
public class LanguePrefListAdapter extends RecyclerView.Adapter<LanguePrefListAdapter.LanguePrefViewHolder>
        implements LangueListAdapter.OnLangueChoisiListener
{

    //Attributs
    private LayoutInflater languePrefInflater;
    private List<Langues> languesList;
    private Resources ressource = null;
    private String pack = "";



    //Constructeur initialisant l'adapteur.
    public LanguePrefListAdapter(Context context) {
        Init(context);
    }

    //Initialise les attributs
    public void Init(Context context)
    {
        languePrefInflater = LayoutInflater.from(context);
        ressource = languePrefInflater.getContext().getResources();
        setLanguesList(new ArrayList<Langues>());
        pack = languePrefInflater.getContext().getPackageName();
    }


    //Appelé à la création des conteneurs de vues.
    @NonNull
    @Override
    public LanguePrefViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = languePrefInflater.inflate(R.layout.langue_choisie, parent, false);
        return new LanguePrefViewHolder(view);
    }

    //Gère l'affichage des drapeaux
    @Override
    public void onBindViewHolder(@NonNull LanguePrefViewHolder holder, int position) {

        //Binding du ViewHolder
            Langues current = getLanguesList().get(position);
            String codeD = current.getCodeDrapeau();

            int id = ressource.getIdentifier(codeD, "drawable", pack);

            holder.drapeauImageView.setImageResource(id);
            holder.langueTextView.setText(current.getLangue());
            holder.paysTextView.setText(current.getPays());
    }

    //Récupère le nombre de langues affichées
    @Override
    public int getItemCount() {
        if (getLanguesList() != null)
            return getLanguesList().size();
        else return 0;

    }


    //Appelé lorsqu'une langue est choisie
    @Override
    public void onLangueChoisi(Langues langue) {

        if(languesList.contains(langue))
        {
            Toast toast = Toast.makeText(languePrefInflater.getContext(), "La langue est déjà sélectionnée.", Toast.LENGTH_LONG);
            toast.show();
        }else
        {

            getLanguesList().add(langue);
            notifyDataSetChanged();

            //Affichage de l'ajout
            Toast toast = Toast.makeText(languePrefInflater.getContext(), "La langue a bien été ajoutée", Toast.LENGTH_LONG);
            toast.show();

        }
    }

    //Supprime une langue à la position indiquée dans la liste
    public void deleteLangue(int pos)
    {
        getLanguesList().remove(pos);
        notifyDataSetChanged();
    }

    public List<Langues> getLanguesList() {
        return languesList;
    }

    public void setLanguesList(List<Langues> languesList) {
        this.languesList = languesList;
    }

    /**
     * ViewHolder contenant un image d'un drapeau,
     * le nom d'un pay, et le nom de la langue.
     */
    public class LanguePrefViewHolder extends RecyclerView.ViewHolder  {

        //Attributs
        public ImageView drapeauImageView;
        public TextView paysTextView;
        public TextView langueTextView;
        public Button btnMenuContext;


        //Constructeur initialisant le view holder
        public LanguePrefViewHolder(@NonNull final View itemView) {
            super(itemView);

            //Initialise
            Init(itemView);
        }

        //Initialise les attributs du ViewHolder
        public void Init(@NonNull final View itemView){
            drapeauImageView= itemView.findViewById(R.id.img_drapeau);


            paysTextView = itemView.findViewById(R.id.text_pays);
            langueTextView = itemView.findViewById(R.id.text_langue);


            btnMenuContext =itemView.findViewById(R.id.btnMenuContext);
        }

    }


}
