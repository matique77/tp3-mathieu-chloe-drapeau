package ui;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

/***
 * Adapteur de page de type fragment
 * Permet de gérer le changement entre deux tabs contenant
 * chacun un fragment différent.
 *
 */
public class LangueFrmgPagerAdapter extends FragmentPagerAdapter {

    //Attributs
    private List<Fragment> mfragments;
    private String[] mtitles;

    //Constructeur initialisant la liste de fragment
    public LangueFrmgPagerAdapter(FragmentManager fm, List<Fragment> langues, String[] titles){
        super(fm);
        if(fm.getFragments().size() == 0){
            mfragments = langues;
        }else
        {
            mfragments = fm.getFragments();
        }
        mtitles = titles;
    }

    //Récupère un fragment selon sa position i.
    @NonNull
    @Override
    public Fragment getItem(int i) {
        return mfragments.get(i);
    }

    //Récupère le nombre de fragment
    @Override
    public int getCount() {
        return mfragments.size();
    }

    //Récupère le titre affiché dans l'onglet.
    @Override
    public CharSequence getPageTitle(int position) {
        return mtitles[position];
    }

}
