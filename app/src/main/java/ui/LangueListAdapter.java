package ui;

import android.content.Context;
import android.content.res.Resources;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dinfogarneau.cours02e.exercices.tp2_mobile.R;

import java.util.List;

import model.Langues;


/**
 * Adapteur gérant l'affichage et la modification
 * des langues générés dans la liste de langues.
 */
public class LangueListAdapter extends RecyclerView.Adapter<LangueListAdapter.LangueViewHolder> {

    //Attributs
    private  LayoutInflater langueInflater;
    private List<Langues> languesList;
    private Resources ressource = null;
    private String pack = "";
    public  String TextChangerLangue ="";
    public  String TextChangerPays ="";
    private OnLangueChoisiListener callback;
    private boolean estModifiable;
    public IModifierLangue modifierLangueCallback;
    public int clickPosition;



    //Constructeur initialisant l'adapteur.
    public LangueListAdapter(Context context) {
        //Initialisation des attributs
        Init(context);
    }


    //Initialise les attributs
    public void Init(Context context)
    {
        langueInflater = LayoutInflater.from(context);
        ressource = langueInflater.getContext().getResources();
        pack = langueInflater.getContext().getPackageName();
        setEstModifiable(false);
        clickPosition = -1;
    }


    //Appelé à la création des conteneurs de vues.
    @NonNull
    @Override
    public LangueViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = langueInflater.inflate(R.layout.choix_langue, viewGroup, false);
        return new LangueViewHolder(view);

    }

    //Gère l'affichage des drapeaux
    @Override
    public void onBindViewHolder(@NonNull LangueViewHolder langueViewHolder, int position) {

            if(!estModifiable)
            {
                //langueViewHolder.itemView.setVisibility(View.GONE);

                langueViewHolder.paysTextView.setEnabled(false);
                langueViewHolder.paysTextView.setBackground(null);
                langueViewHolder.btnMenuContext.setVisibility(View.GONE);
                langueViewHolder.langueTextView.setEnabled(false);
                langueViewHolder.langueTextView.setBackground(null);
            }
            else
            {
                langueViewHolder.paysTextView.setEnabled(true);
                langueViewHolder.btnMenuContext.setVisibility(View.VISIBLE);
                langueViewHolder.langueTextView.setEnabled(true);
            }

            //Binding du ViewHolder
            Langues current = languesList.get(position);
            String codeD = current.getCodeDrapeau();

            int id = ressource.getIdentifier(codeD, "drawable", pack);

            langueViewHolder.drapeauImageView.setImageResource(id);
            langueViewHolder.langueTextView.setText(current.getLangue());
            langueViewHolder.paysTextView.setText(current.getPays());
            langueViewHolder.btnMenuContext.setEnabled(false);

    }

    //Set la liste de langues
    public void setLangues(List<Langues> langues) {
        languesList = langues;
        notifyDataSetChanged();
    }

    //Set la liste de langues
    public void setUneLangue(int i, Langues langue) {
        languesList.set(i, langue);
        notifyDataSetChanged();
    }

    //Permet de récupérer toutes les langues
    public List<Langues> getLangue() {
        return languesList;
    }

    //Permet de récupérer une langue
    public Langues getUneLangue(int i) {
        return languesList.get(i);
    }


    //Récupère le nombre de langues affichées
    @Override
    public int getItemCount() {
        if (languesList != null)
            return languesList.size();
        else return 0;
    }

    public void setOnLangueChoisiListener(OnLangueChoisiListener listener)
    {
        this.callback = listener;
    }



    public boolean isEstModifiable() {
        return estModifiable;
    }

    public void setEstModifiable(boolean estModifiable) {
        this.estModifiable = estModifiable;
        notifyDataSetChanged();
    }


    //Interface gérant l'ajout des langues choisis à l'autre liste
    public interface OnLangueChoisiListener
    {
        void onLangueChoisi(Langues langue);
    }

    /**
     * ViewHolder contenant un image d'un drapeau,
     * le nom d'un pays, et le nom de la langue.
     *
     * Gère aussi les évènements commes les cliques et
     * les modifications.
     */
    public class LangueViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener, View.OnClickListener, View.OnTouchListener {

        //Attributs
        public ImageView drapeauImageView;
        public TextView paysTextView;
        public TextView langueTextView;
        public Button btnMenuContext;
        public View message;



        //Constructeur initialisant le view holder
        public LangueViewHolder(@NonNull final View itemView) {
            super(itemView);

            //Initialisation des attributs
            Init(itemView);



                langueTextView.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        //Do nothing
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                        TextChangerLangue = editable.toString();

                    }
                });

                View.OnFocusChangeListener onFocusListener = new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean b) {
                        if (!b) {
                            btnMenuContext.setEnabled(false);
                        } else {
                            TextChangerPays = paysTextView.getText().toString().trim();
                            TextChangerLangue = langueTextView.getText().toString().trim();
                            btnMenuContext.setEnabled(true);
                        }

                    }
                };

                paysTextView.setOnFocusChangeListener(onFocusListener);
                langueTextView.setOnFocusChangeListener(onFocusListener);

                paysTextView.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        int i = LangueViewHolder.super.getAdapterPosition();
                        Langues langue = getUneLangue(i);
                        TextChangerPays = editable.toString();

                    }
                });

            }


        //Initialise les attributs du ViewHolder
        public void Init(@NonNull final View itemView) {
            drapeauImageView= itemView.findViewById(R.id.img_drapeau);

            itemView.setOnTouchListener(this);

            paysTextView =  itemView.findViewById(R.id.text_pays);

            langueTextView = itemView.findViewById(R.id.text_langue);

            message =  itemView.findViewById(R.id.layoutMessage);

            btnMenuContext = itemView.findViewById(R.id.btnMenuContext);
            btnMenuContext.setOnClickListener(this);



        }

        //Gère l'ajout d'un bouton Modifier
        @Override
        public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            menu.setHeaderTitle("...");
            menu.add(this.getAdapterPosition(),2,0,"Modifier");
        }



        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if(  motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                view.setAlpha(0.6f);
                return  true;
            }
            else if(motionEvent.getAction() == MotionEvent.ACTION_UP)
            {
                view.setAlpha(1.0f);
                int i = getAdapterPosition();
                callback.onLangueChoisi(languesList.get(i));
            }
            else
            {
                view.setAlpha(1.0f);
            }
            return false;
        }

        @Override
        public void onClick(View view) {

            if(view.getId() == R.id.btnMenuContext )
            {
                int i = getAdapterPosition();
                modifierLangueCallback.ModifierLangue(i);

            }
        }
    }





}
