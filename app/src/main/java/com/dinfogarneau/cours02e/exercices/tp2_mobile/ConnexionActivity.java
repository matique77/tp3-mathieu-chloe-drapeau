package com.dinfogarneau.cours02e.exercices.tp2_mobile;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;


public class ConnexionActivity extends AppCompatActivity implements View.OnClickListener, OnCompleteListener<AuthResult> {
    //Gère la création de l'activité
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Initialisation de la vue
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connexion);
        findViews();
        mAuth = FirebaseAuth.getInstance();
        mEstAdmin = false;
    }

    //Attributs privés
    private EditText etEmail;
    private EditText etMp;
    private Button btnConnexion;
    private boolean mEstAdmin;


    //Instance de la bd
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference collUtilisteur = db.collection("utilisateurs");
    private FirebaseAuth mAuth;

    //Clés
    public static final String cle_admin = "EST_ADMIN";

    //Seed
    /*Permet la création des utilisateurs et leur asigne un rôle*/
    //public  void Seed() {
    //    mAuth.createUserWithEmailAndPassword(getString(R.string.conn_admin), getString(R.string.conn_mdp));
    //    mAuth.createUserWithEmailAndPassword(getString(R.string.conn_client), getString(R.string.conn_mdp));
    //}


    //Listeners
    @Override
    public void onComplete(@NonNull Task<AuthResult> task) {
        if (task.isSuccessful()) {
            Toast.makeText(this.getApplicationContext(), "Connecté!",Toast.LENGTH_LONG).show();
            // Connexion avec succès
            FirebaseUser user = mAuth.getCurrentUser();

            //Doit vérifier si l'utilisateur est admin.
            db.collection("utilisateurs").whereEqualTo("idUser",user.getUid()).get()
                    .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                @Override
                public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                    List<DocumentSnapshot> docs =  queryDocumentSnapshots.getDocuments();
                    if(docs.get(0) != null)
                    {
                        UserInfo userInfo = docs.get(0).toObject(UserInfo.class);
                        mEstAdmin = userInfo.getEstAdmin();
                    }

                    //On charge l'activité principale :
                    Intent intent = new Intent(getBaseContext(), MainActivity.class);
                    intent.putExtra(cle_admin, mEstAdmin);


                    startActivity(intent);
                }
            });

        }
        else {
            Toast.makeText(this.getApplicationContext(), "Le nom d'utilisateur relié au mot de passe que vous avez entré n'existe pas.",Toast.LENGTH_LONG).show();
        }
    }


    private void findViews() {
        etEmail = (EditText)findViewById( R.id.etEmail );
        etMp = (EditText)findViewById( R.id.etMp );
        btnConnexion = (Button)findViewById( R.id.btnConnexion );

        btnConnexion.setOnClickListener( this );
    }


    //Gestion des clicks
    @Override
    public void onClick(View v) {
        if ( v == btnConnexion ) {
           //On intente une connexion avec Firebase
            String courriel = etEmail.getText().toString().toLowerCase().trim();
            String mdp = etMp.getText().toString();

            if (!TextUtils.isEmpty(courriel) && !TextUtils.isEmpty(mdp)) {
                LoginUser(courriel, mdp);
            }
            else
                {
                    if(TextUtils.isEmpty(courriel))
                    {
                        Toast.makeText(this.getApplicationContext(), "Champ courriel vide",Toast.LENGTH_LONG).show();
                        etEmail.setFocusable(View.FOCUSABLE);
                    }

                    if(TextUtils.isEmpty(mdp))
                    {
                        Toast.makeText(this.getApplicationContext(), "Champ mot de passe vide",Toast.LENGTH_LONG).show();
                        etMp.setFocusable(View.FOCUSABLE);
                    }
                }


        }


        }
    //Méthodes de connexion
    private void LoginUser(String email, String mdp){
        mAuth.signInWithEmailAndPassword(email, mdp).addOnCompleteListener(this, this);
    }


}

