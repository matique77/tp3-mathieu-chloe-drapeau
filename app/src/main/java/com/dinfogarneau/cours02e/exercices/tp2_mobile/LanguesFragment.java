package com.dinfogarneau.cours02e.exercices.tp2_mobile;

import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import model.Langues;
import ui.IModifierLangue;
import ui.LangueListAdapter;

/**
 * Un fragment permettant la gestion d'une liste de langues liées à un code
 * de pays.
 * Permet de créer, modifier et supprimer des langues ajoutées à une liste.
 *
 * Note : Ajoute les éléments à une seconde liste.
 * (Voir LanguesPreFragment)
 */
public class LanguesFragment extends ListFragment implements View.OnClickListener, IModifierLangue {

    //Attributs de la bd
    private CollectionReference collRefLangue;
    private FirebaseFirestore mDb;

    // Attributs du fragment
    private View liste;
    private RecyclerView langueRecListeView;
    private LangueListAdapter  langueListAdapter;
    private FloatingActionButton btn_ajout;
    private EditText codeD;
    private EditText langue;
    private EditText pays;
    private OnComplete callBack;
    private View lstAjout;


    //Gère la cration du la vue
    //NOTE: Le fragment n'est pas attaché au root car on souhaite gérer les onglets.
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_langues_list, container, false);
    }

    //Initialisation des données et des vues.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        //Initialise les attributs
        Init(view);

        //Initialise la base de données
        InitBD();

        //On ajoute le listener pour mettre à jour à chaque en direct
        collRefLangue.addSnapshotListener( new EventListener<QuerySnapshot>() {

            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.d("DB", "Il y a une erreur avec l'écouteur d'événement");
                    return;
                }

                //On remet à jour la liste
                getAllLangues();
            }

        });

        //On set l'évènement complété
        setCallBack((MainActivity) getActivity());
        getCallBack().onComplete(0);


    }


    //Gère l'initialisation des attributs
    public void Init(View view)
    {
        // Initialise les attributs
        langue= view.findViewById(R.id.editText_langue);
        pays= view.findViewById(R.id.editText_pays);
        codeD =view.findViewById(R.id.editText_codeD);
        btn_ajout = view.findViewById(R.id.btn_ajout);
        btn_ajout.setOnClickListener(this);
        lstAjout = view.findViewById(R.id.lstAjout);



        //Initialisation du RecyclerView
        langueRecListeView = view.findViewById(android.R.id.list);
        langueRecListeView.setLayoutManager(new LinearLayoutManager(getActivity()));
        setLangueListAdapter(new LangueListAdapter(getContext()));
        langueRecListeView.setAdapter(getLangueListAdapter());

        //Gestion du swipe
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            // Appelé lorsque l'élément se déplace
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            // Appelé une fois le swipe effectué
            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, int swipeDir) {
                if(((MainActivity)getActivity()).getIsAdmin())
                {
                    int position = viewHolder.getAdapterPosition();
                    List<Langues> langues = getLangueListAdapter().getLangue();
                    deleteLangue(langues.get(position));
                }
            }


            // Gestion de l'apparence de la suppresion
            @Override
            public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                if(getEnableList())
                {
                    new it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                            .addSwipeLeftBackgroundColor(Color.RED)
                            .addSwipeRightBackgroundColor(Color.RED)
                            .addSwipeLeftActionIcon(R.drawable.ic_delete_sweep_black_24dp)
                            .addSwipeRightActionIcon(R.drawable.ic_delete_sweep_black_24dp)

                            .create()
                            .decorate();
                    super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                }

            }
        }).attachToRecyclerView(langueRecListeView);

        this.getLangueListAdapter().modifierLangueCallback = this;

    }

    //Gère l'initialisation des données
    public void InitBD()
    {
        mDb = FirebaseFirestore.getInstance();
        collRefLangue = mDb.collection("langues");
    }



    //Set le mode admin
    public void setEnableList(boolean enable)
    {

        int visibilite = enable ? View.VISIBLE : View.GONE;

        codeD.setVisibility(visibilite);
        langue.setVisibility(visibilite);
        pays.setVisibility(visibilite);
        btn_ajout.setVisibility(visibilite);
        lstAjout.setVisibility(visibilite);
        codeD.setEnabled(enable);
        langue.setEnabled(enable);
        pays.setEnabled(enable);
        btn_ajout.setEnabled(enable);
    }

    //Set le mode admin
    public boolean getEnableList()
    {
        return btn_ajout.isEnabled();
    }


    //Gère l'ajout des langues crées si elles sont valides
    @Override
    public void onClick(final View view) {

        switch (view.getId()) {

            case R.id.btn_ajout:
                //On vérifie que le pays est valide :
                String strCodePays = codeD.getText().toString().trim();
                String strPays = pays.getText().toString().trim();
                String strLangue = langue.getText().toString().trim();

                Toast toast;

                if( !strCodePays.isEmpty() &&
                        !strPays.isEmpty()  &&
                        !strLangue.isEmpty())
                {

                    Langues unelangue = new Langues( strCodePays.trim().toLowerCase(), strPays, strLangue);


                    addLangue(unelangue);

                }
                else
                {
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast toast = Toast.makeText(getContext(), "La langue est invalide", Toast.LENGTH_LONG);
                            toast.show();
                        }
                    });

                }


        }

    }

    public void updateLangue(final Langues ancienneLangue, final Langues nouvelleLangue)
    {
        collRefLangue.whereEqualTo("langue", nouvelleLangue.getLangue()).whereEqualTo("pays", nouvelleLangue.getPays()).get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                        if(queryDocumentSnapshots.isEmpty())
                        {
                            collRefLangue.whereEqualTo("langue", ancienneLangue.getLangue()).whereEqualTo("pays", ancienneLangue.getPays()).get()
                                    .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                                        @Override
                                        public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                            String id = queryDocumentSnapshots.getDocuments().get(0).getId();


                                            DocumentReference test = collRefLangue.document(id);
                                            //Mise à jour d'une langue
                                            collRefLangue.document(id).update("langue", nouvelleLangue.getLangue());
                                            collRefLangue.document(id).update("codeDrapeau", nouvelleLangue.getCodeDrapeau());
                                            collRefLangue.document(id).update("pays", nouvelleLangue.getPays());

                                            getLangueListAdapter().notifyDataSetChanged();

                                            //Affichage des modifications
                                            Toast toast = Toast.makeText(getContext(), "La langue a bien été modifiée!", Toast.LENGTH_LONG);
                                            toast.show();
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.d("DB", "Problème avec la bd.");

                                        }
                                    });
                        }
                        else
                        {
                            Toast toast = Toast.makeText(getContext(), "La langue existe déjà!", Toast.LENGTH_LONG);
                            toast.show();

                            getAllLangues();

                        }
                    }
                });

    }

    public void addLangue(final Langues langues)
    {
        //Avant de l'ajouter on doit vérifier si elle existe
        //On get la langue
        collRefLangue.whereEqualTo("langue", langues.getLangue()).whereEqualTo("pays", langues.getPays()).get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        if(queryDocumentSnapshots.getDocuments().isEmpty())
                        {
                            collRefLangue.add(langues);
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast toast = Toast.makeText(getContext(), "La langue a bien été ajoutée à la liste", Toast.LENGTH_LONG);
                                    toast.show();
                                }
                            });
                        }
                        else
                        {
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast toast = Toast.makeText(getContext(), "La langue est déjà contenu dans la liste.", Toast.LENGTH_LONG);
                                    toast.show();
                                }
                            });
                        }
                    }

                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("DB", "Problème avec la bd.");

                    }
                });



    }


    public void deleteLangue(Langues langues)
    {
        //On get la langue
        collRefLangue.whereEqualTo("langue", langues.getLangue()).whereEqualTo("pays", langues.getPays()).get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                      String id = queryDocumentSnapshots.getDocuments().get(0).getId();
                      collRefLangue.document(id).delete();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("DB", "Problème avec la bd.");

                    }
                });
    }


    public void getAllLangues()
    {
        collRefLangue.get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                        ArrayList<Langues> langues =  new ArrayList<Langues>();

                        for (QueryDocumentSnapshot snapshots: queryDocumentSnapshots) {
                            langues.add(snapshots.toObject(Langues.class));
                        }

                        //On ajoute des langues
                        getLangueListAdapter().setLangues(langues);

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("DB", "Problème avec la bd.");
                    }
                });
    }




    //Propriétés

    public LangueListAdapter getLangueListAdapter() {
        return langueListAdapter;
    }

    public void setLangueListAdapter(LangueListAdapter langueListAdapter) {
        this.langueListAdapter = langueListAdapter;
    }

    public OnComplete getCallBack() {
        return callBack;
    }

    public void setCallBack(OnComplete callBack) {
        this.callBack = callBack;
    }

    @Override
    public void ModifierLangue(int id) {
        LangueListAdapter adapter = getLangueListAdapter();

        Langues ancienneLangue = adapter.getUneLangue(id);

        Langues nouvelleLangue =  new Langues(ancienneLangue.getCodeDrapeau(),
                adapter.TextChangerPays,  adapter.TextChangerLangue);

        // Valide que le edit text n'est pas vide.
        if(!nouvelleLangue.getPays().trim().isEmpty() &&
                !nouvelleLangue.getLangue().trim().isEmpty())
        {
            updateLangue(ancienneLangue, nouvelleLangue);
        } else
        {
            Toast toast = Toast.makeText(getContext(), "La langue est invalide", Toast.LENGTH_LONG);
            toast.show();

            getAllLangues();

            adapter.notifyDataSetChanged();
        }


    }
}
