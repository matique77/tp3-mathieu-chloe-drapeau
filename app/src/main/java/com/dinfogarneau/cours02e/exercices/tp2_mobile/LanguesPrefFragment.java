package com.dinfogarneau.cours02e.exercices.tp2_mobile;

import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.ListFragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import model.Langues;
import ui.LanguePrefListAdapter;


/**
 * Un fragment représentant une liste de langue préférées
 * Permet de supprimer les langues contenues dans la liste.
 *
 * Note : Les éléments sont ajoutés grâce à une liste de langues.
 * (Voir LanguesFragment)
 */
public class LanguesPrefFragment extends ListFragment {

    // Attributs du fragment
    private RecyclerView langueRecListeView;
    private LanguePrefListAdapter langueListAdapter;
    private OnComplete callBack;

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        //On enregistre les éléments de la liste
        outState.putStringArray( getString(R.string.cle_liste) , listLanguesToArray());

    }

    //Permet la sauvegarde de la liste dans une série de string
    //représentant les langues.
    private String[] listLanguesToArray()
    {
        List<Langues> lstLangues = getLangueListAdapter().getLanguesList();
        String[] liste = new String[lstLangues.size()];

        for (int i = 0; i < lstLangues.size() ; i++)
        {
            liste[i] =  Langues.langueToStr(lstLangues.get(i));
        }

        return liste;
    }

    //Récupère la série de string et la converti en liste de de langues.
    private List<Langues> arrayLanguesToList(Bundle state) {
        String[] arrayLangues =  state.getStringArray(getString(R.string.cle_liste));
        List<Langues> lstLangues = new ArrayList<Langues>();

        for(int i = 0 ; i < arrayLangues.length ; i++ )
        {
            lstLangues.add(Langues.strToLangue(arrayLangues[i]));
        }

        return lstLangues;

    }


    //Gère la création de la vue
    //NOTE: Le fragment n'est pas attaché au root car on souhaite gérer les onglets.
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_langues_pref, container, false);
    }

    //Initialisation des vues
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        //Initialise les attributs
        Init(view, savedInstanceState);

        //On set l'évènement complété
        setCallBack((MainActivity) getActivity());
        getCallBack().onComplete(1);
    }

    //Gère l'initialisation des attributs
    public void Init (View view, Bundle savedInstanceState)
    {
        //Initialisation du RecyclerView
        langueRecListeView = view.findViewById(android.R.id.list);
        langueRecListeView.setLayoutManager(new LinearLayoutManager(getActivity()));
        setLangueListAdapter(new LanguePrefListAdapter(getContext()));
        langueRecListeView.setAdapter(getLangueListAdapter());

        //On vérifie si la liste était précédemment remplie,
        //Si oui on la récupère.
        if(savedInstanceState != null){
            List<Langues> lst = arrayLanguesToList(savedInstanceState);
            if(lst != null)
            {
                getLangueListAdapter().setLanguesList(lst);
                getLangueListAdapter().notifyDataSetChanged();
            }
        }


        //Gestion du swipe
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            // Appelé lorsque l'élément se déplace
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            // Appelé une fois le swipe effectué
            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int position = viewHolder.getAdapterPosition();
                getLangueListAdapter().deleteLangue(position);
            }

            // Gestion de l'apparence de la suppresion
            @Override
            public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                new it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                        .addSwipeLeftBackgroundColor(Color.RED)
                        .addSwipeRightBackgroundColor(Color.RED)
                        .addSwipeLeftActionIcon(R.drawable.ic_delete_sweep_black_24dp)
                        .addSwipeRightActionIcon(R.drawable.ic_delete_sweep_black_24dp)

                        .create()
                        .decorate();
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        }).attachToRecyclerView(langueRecListeView);


        //Communication entre les deux fragments
        MainActivity act =(MainActivity) getActivity();
        LanguesFragment langueF = (LanguesFragment) act.getmLanguePagerAdapter().getItem(0);
        LanguesPrefFragment languePref = (LanguesPrefFragment) act.getmLanguePagerAdapter().getItem(1);
        langueF.getLangueListAdapter().setOnLangueChoisiListener(languePref.getLangueListAdapter());
    }


    //Propriétés
    public LanguePrefListAdapter getLangueListAdapter() {
        return langueListAdapter;
    }

    public void setLangueListAdapter(LanguePrefListAdapter langueListAdapter) {
        this.langueListAdapter = langueListAdapter;
    }

    public OnComplete getCallBack() {
        return callBack;
    }

    public void setCallBack(OnComplete callBack) {
        this.callBack = callBack;
    }
}
