package com.dinfogarneau.cours02e.exercices.tp2_mobile;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;

import ui.LangueFrmgPagerAdapter;


/**
 * Une activité comportant deux fragments de type liste.
 *
 * Le premier est liées à une base de données d'ajouter des
 * langues de préférances.
 *
 * Le deuxième est une liste de langues de préférances choisies.
 *
 * On navigue entre les fragments à l'aide d'onglet.
 */
public class MainActivity extends AppCompatActivity
    implements OnComplete
{

    //Attributs (Gestion des onglets)
    private ViewPager mViewPager;
    private LangueFrmgPagerAdapter mLanguePagerAdapter;
    private TabLayout mTabLayout;

    //Attributs (Gestion du mode admin)
    private boolean isAdmin;

    //Utilisateur connecté
    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;


    //Gère la sauvegarde de l'état admin
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        //On save le mode admin
        outState.putInt(getString(R.string.cle_admin), getIsAdmin() ? 1 : 0 );

    }

    //Gère la création de l'activité
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Initialisation de la vue
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Récupère l'utilisateur connecté
        mAuth = FirebaseAuth.getInstance();
        mCurrentUser = mAuth.getCurrentUser();

        //Initialisation du ViewPager
        onCreateViewAdapter(savedInstanceState);

        //Initialisation des Tabs
        onCreateTabLayout();
    }

    //Gère la création du menu
    public void onCreateMenu(Menu menu)
    {
        //Initialisation du menu
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
    }

    //Initialisation du ViewPager
    public void onCreateViewAdapter(Bundle savedInstanceState)
    {
        //Création d'une liste de fragment contenant une référence
        //à nos deux fragment de type liste.
        List<Fragment> langues;
        langues = new ArrayList<Fragment>();
        String[] titres = getResources().getStringArray(R.array.title_array);

        langues.add(new LanguesFragment());
        langues.add(new LanguesPrefFragment());

        //Initialisation de l'adapter
        setmLanguePagerAdapter(new LangueFrmgPagerAdapter(getSupportFragmentManager(), langues, titres));

        //Set l'adapter au ViewPager
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        mViewPager.setAdapter(getmLanguePagerAdapter());

        //Récupère l'utilisateur connecté
        mAuth = FirebaseAuth.getInstance();
        mCurrentUser = mAuth.getCurrentUser();

        //Assigne si admin ou non
        setIsAdmin(getIntent().getBooleanExtra(ConnexionActivity.cle_admin, false));

    }


    //Initialisation des Tabs
    public void onCreateTabLayout()
    {
        //Set le Tab layout
        mTabLayout = (TabLayout) findViewById(R.id.tabLayout);
        mTabLayout.setupWithViewPager(mViewPager);
    }




    //Appelé lorsqu'un fragment est prêt
    @Override
    public void onComplete(int id) {
        //Lorsque le deuxième fragment est prêt
        if(id == 1)
        {
            //Communication entre les fragments
            getLanguesF().getLangueListAdapter().setOnLangueChoisiListener(getLanguesPrefFragment().getLangueListAdapter());

            //On change l'accès à la liste
            getLanguesF().setEnableList(isAdmin);

            //On change l'apparence de la liste
            getLanguesF().getLangueListAdapter().setEstModifiable(isAdmin);

        }
    }



    //Propriétés

    public boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean admin) {
        isAdmin = admin;

    }

    public LanguesFragment getLanguesF() {
        return (LanguesFragment) mLanguePagerAdapter.getItem(0);
    }


    public LanguesPrefFragment getLanguesPrefFragment() {
        return (LanguesPrefFragment) mLanguePagerAdapter.getItem(1);
    }


    public LangueFrmgPagerAdapter getmLanguePagerAdapter() {
        return mLanguePagerAdapter;
    }

    public void setmLanguePagerAdapter(LangueFrmgPagerAdapter mLanguePagerAdapter) {
        this.mLanguePagerAdapter = mLanguePagerAdapter;
    }
}