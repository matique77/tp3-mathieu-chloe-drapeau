package com.dinfogarneau.cours02e.exercices.tp2_mobile;

public class UserInfo {

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public boolean getEstAdmin() {
        return estAdmin;
    }

    public void setEstAdmin(boolean estAdmin) {
        this.estAdmin = estAdmin;
    }

    private String idUser;
    private boolean estAdmin;

    public UserInfo(){

    }

    public UserInfo(String idUser, Boolean estAdmin){
        this.idUser = idUser;
        this.estAdmin = estAdmin;
    }


}
