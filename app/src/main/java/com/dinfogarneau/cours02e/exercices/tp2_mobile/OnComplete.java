package com.dinfogarneau.cours02e.exercices.tp2_mobile;

/**
 * Interface gérant le moment ou les deux fragments sont prêt.
 */
public interface OnComplete{
    void onComplete(int id);
}